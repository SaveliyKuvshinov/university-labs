#include "LongNumber.h"
#include <string>

using namespace std;

LongNumber::LongNumber(string str)
{
    positive = true;

    if (str[0] == '-') {
        positive = false;
        str = str.substr(1);
    }

    int inputLength = str.length();
    numberLength = inputLength / baseDigitLength + (inputLength % baseDigitLength == 0 ? 0 : 1);
    number = new int[numberLength];
    int numberIndex = 0;
    int inputIndex = inputLength - baseDigitLength;

    while (inputIndex > 0) {
        number[numberIndex] = stoi(str.substr(inputIndex, baseDigitLength));
        inputIndex -= baseDigitLength;
        numberIndex++;
    }

    number[numberIndex] = stoi(str.substr(0, inputIndex + baseDigitLength));
}


LongNumber::LongNumber(const int *arr, int newNumberLength, bool newPositive) {
    positive = newPositive;
    numberLength = newNumberLength;
    number = new int[numberLength];
    for (int i = 0; i < numberLength; i++) {
        number[i] = arr[i];
    }
}

bool LongNumber::getPositive()
{
    return positive;
}

void LongNumber::setPositive(bool sign)
{
    positive = sign;
}

int LongNumber::getNumberLength() {
    return numberLength;
}

int LongNumber::getBase() {
    return base;
}
