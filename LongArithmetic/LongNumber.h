#include <string>

using namespace std;

class LongNumber {
private:
    const int base = 10000;
    const int baseDigitLength = 4;
    int numberLength;
    bool positive;

public:
    explicit LongNumber(string str);
    LongNumber(const int* arr, int newNumberLength, bool newPositive);
    void setPositive(bool positive);
    bool getPositive();
    int getNumberLength();
    int getBase();
    int* number;
};
