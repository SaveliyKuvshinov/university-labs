#include <iostream>
#include <fstream>
#include <string>
#include "LongNumber.h"

using namespace std;

int compareLongNumbers(LongNumber* number1, LongNumber* number2)
{
    int numberLength1 = number1->getNumberLength();
    int numberLength2 = number2->getNumberLength();

    if (numberLength1 == numberLength2) {

        int index = numberLength1 - 1;
        while (index >= 0 && number1->number[index] == number2->number[index]) {
            index--;
        }

        if (index < 0) {
            return 0;
        }

        return number1->number[index] > number2->number[index] ? 1 : -1;
    }
    else {
        return numberLength1 > numberLength2 ? 1 : -1;
    }
}

LongNumber* absSubtraction(LongNumber* reduced, LongNumber* subtracted)
{
    int reducedLength = reduced->getNumberLength();
    int subtractedLength = subtracted->getNumberLength();
    int result[reducedLength];
    int base = reduced->getBase();
    int subValue = 0;

    for (int i = 0; i < subtractedLength; i++) {
        int difference = reduced->number[i] - subtracted->number[i] - subValue;
        if (difference < 0) {
            result[i] = difference + base;
            subValue++;
        }
        else {
            result[i] = difference;
            subValue = 0;
        }
    }

    for (int i = subtractedLength; i < reducedLength; i++) {
        int difference = reduced->number[i] - subValue;
        if (difference < 0) {
            result[i] = difference + base;
            subValue = 1;
        }
        else {
            result[i] = difference;
            subValue = 0;
        }
    }

    int zeroCounter = reducedLength - 1;
    while (result[zeroCounter] == 0) {
        zeroCounter--;
    }

    return new LongNumber(result, zeroCounter + 1, true);

}

LongNumber* absAddition(LongNumber* number1, LongNumber* number2)
{
    int numberLength1 = number1->getNumberLength();
    int numberLength2 = number2->getNumberLength();
    int maxNumberLength = max(numberLength1, numberLength2);
    int minNumberLength = min(numberLength1, numberLength2);

    LongNumber* maxNumber = maxNumberLength == numberLength1 ? number1 : number2;
    int result[maxNumberLength + 1];
    int base = number1->getBase();
    int addValue = 0;

    for (int i = 0; i < minNumberLength; i++) {
        int sum = number1->number[i] + number2->number[i] + addValue;
        result[i] = sum % base;
        addValue = sum / base;
    }

    if (maxNumberLength > minNumberLength) {
        for (int i = minNumberLength; i < maxNumberLength; i++) {
            int sum = maxNumber->number[i] + addValue;
            result[i] = sum % base;
            addValue = sum / base;
        }
    }

    int newNumberLength = maxNumberLength;
    if (addValue > 0) {
        result[newNumberLength] = addValue;
        newNumberLength++;
    }

    return new LongNumber(result, newNumberLength, true);
}

LongNumber* addition(LongNumber* number1, LongNumber* number2)
{
    if (number1->getPositive() == number2->getPositive()) {
        LongNumber* newNumber = absAddition(number1, number2);
        newNumber->setPositive(number1->getPositive());
        return newNumber;
    }
    else {
        switch (compareLongNumbers(number1, number2)) {
            case 1: {
                LongNumber* newNumber = absSubtraction(number1, number2);
                newNumber->setPositive(number1->getPositive());
                return newNumber;
            }
            case -1: {
                LongNumber* newNumber = absSubtraction(number2, number1);
                newNumber->setPositive(!number1->getPositive());
                return newNumber;
            }
            default: {
                int zeroArr[1] = {0};
                return new LongNumber(zeroArr, 1, true);
            }
        }
    }
}

LongNumber* subtraction(LongNumber* number1, LongNumber* number2)
{
    if (number1->getPositive() != number2->getPositive()) {
        LongNumber* newNumber = absAddition(number1, number2);
        newNumber->setPositive(number1->getPositive());
        return newNumber;
    }
    else {
        switch (compareLongNumbers(number1, number2)) {
            case 1: {
                LongNumber* newNumber = absSubtraction(number1, number2);
                newNumber->setPositive(number1->getPositive());
                return newNumber;
            }
            case -1: {
                LongNumber* newNumber = absSubtraction(number2, number1);
                newNumber->setPositive(!number1->getPositive());
                return newNumber;
            }
            default: {
                int zeroArr[1] = {0};
                return new LongNumber(zeroArr, 1, true);
            }
        }
    }
}

LongNumber* multiplication(LongNumber* number1, LongNumber* number2)
{
    int numberLength1 = number1->getNumberLength();
    int numberLength2 = number2->getNumberLength();
    int base = number1->getBase();
    int addValue = 0;
    LongNumber* terms[numberLength1];

    for (int i = 0; i < numberLength2; i++) {
        int termNumberLength = numberLength1 + 1 + i;
        int termArray[termNumberLength];

        for (int k = 0; k < numberLength1; k++) {
            int multiple = number1->number[k] * number2->number[i] + addValue;
            termArray[k + i] = multiple % base;
            addValue = multiple / base;
        }

        if (termArray[numberLength1 + i] == 0) {
            termNumberLength--;
        }
        terms[i] = new LongNumber(termArray, termNumberLength, true);
    }

    LongNumber* result = terms[0];
    for (int i = 1; i < numberLength1; i++) {
        result = addition(result, terms[i]);
    }

    result->setPositive(number1->getPositive() == number2->getPositive());
    return result;
}

string getBaseDigitString(int number, int base)
{
    string result;
    int digit;
    do {
        base /= 10;
        digit = number / base;
        result += to_string(digit);
        number %= base;
    } while (base > 1);

    return result;
}

void processOutput(LongNumber* number)
{
    ofstream file("out.txt");
    if (file.is_open()) {
        if (!number->getPositive()) {
            file << '-';
        }

        for (int i = number->getNumberLength() - 1; i >= 0; i--) {
            file << getBaseDigitString(number->number[i], number->getBase());
        }
        file.close();
    }
    else {
        cout << "error!";
    }
}

void processInput(const string& inputFileName)
{
    ifstream file(inputFileName);
    if (file.is_open()) {
        string str1, str2;
        getline(file, str1);
        getline(file, str2);

        auto number1 = new LongNumber(str1);
        auto number2 = new LongNumber(str2);
        auto number3 = multiplication(number1, number2);
        processOutput(number3);

        file.close();
    }
    else {
        cout << "Cannot open file with such name" << endl;
    }
}

int main() {
    processInput("input.txt");
    return 0;
}