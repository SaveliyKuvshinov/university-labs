#include <iostream>
#include <conio.h>
#include <ctime>
#include <windows.h>

#define LIGHT_BLUE 3
#define RED 12
#define DEFAULT 7
#define GREEN 10

using namespace std;

void mySwap(int &x, int &y) {
    int c = x;
    x = y;
    y = c;
}

void printArrayByIndexesAndColor(int arr[], int size, int index1, int index2, int color) {
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

    Sleep(1000);
    for (int k = 0; k < size; k++) {
        if (k == index1 || k == index2) {
            SetConsoleTextAttribute(hConsole, color);
            cout << arr[k] << " ";
            SetConsoleTextAttribute(hConsole, DEFAULT);
        }
        else {
            cout << arr[k] << " ";
        }
    }
    cout << endl;
}

void printArrayByRangeAndColor(int arr[], int size, int startIndex, int endIndex, int color) {
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    Sleep(1000);
    for (int k = 0; k < size; k++) {
        if (k >= startIndex && k <= endIndex) {
            SetConsoleTextAttribute(hConsole, color);
            cout << arr[k] << " ";
            SetConsoleTextAttribute(hConsole, DEFAULT);
        }
        else {
            cout << arr[k] << " ";
        }
    }
    cout << endl;
}

void selectionSort(int arr[], int size) {
    int minIndex;
    cout << "Selection sort" << endl << endl;

    for (int i = 0; i < size - 1; i++) {
        minIndex = i;
        cout << "i = " << i << ":" << endl;

        for (int k = i + 1; k < size; k++) {
            if (arr[minIndex] > arr[k]) {
                minIndex = k;
            }
        }

        printArrayByIndexesAndColor(arr, size, i, minIndex, RED);
        mySwap(arr[i], arr[minIndex]);
        printArrayByIndexesAndColor(arr, size, i, minIndex, LIGHT_BLUE);
        cout << endl;
    }
}

void insertionSort(int arr[], int size) {
    int insertIndex, insertElement;
    cout << "Insertion sort" << endl << endl;

    for (int i = 1; i < size; i++) {
        insertIndex = i;
        insertElement = arr[i];
        cout << "i = " << i << ":" << endl;

        while (insertIndex > 0 && arr[insertIndex - 1] > insertElement) {
            printArrayByIndexesAndColor(arr, size, insertIndex, insertIndex - 1, RED);
            arr[insertIndex] = arr[insertIndex - 1];
            printArrayByIndexesAndColor(arr, size, insertIndex, insertIndex - 1, LIGHT_BLUE);
            insertIndex--;
        }

        printArrayByIndexesAndColor(arr, size, insertIndex,-1, RED);
        arr[insertIndex] = insertElement;
        printArrayByIndexesAndColor(arr, size, insertIndex,-1, LIGHT_BLUE);
        cout << endl;
    }
}

void merge(int arr[], int left, int middle, int right) {
    int
        leftArrSize = middle - left + 1,
        rightArrSize = right - middle,
        leftArr[leftArrSize],
        rightArr[rightArrSize],
        leftIndex = 0,
        rightIndex = 0,
        mergeIndex = left;

    for (int i = 0; i < leftArrSize; i++) {
        leftArr[i] = arr[left + i];
    }
    for (int k = 0; k < rightArrSize; k++) {
        rightArr[k] = arr[middle + 1 + k];
    }

    while (leftIndex < leftArrSize && rightIndex < rightArrSize) {
        if (leftArr[leftIndex] > rightArr[rightIndex]) {
            arr[mergeIndex] = rightArr[rightIndex];
            rightIndex++;
        }
        else {
            arr[mergeIndex] = leftArr[leftIndex];
            leftIndex++;
        }
        mergeIndex++;
    }

    while (leftIndex < leftArrSize) {
        arr[mergeIndex] = leftArr[leftIndex];
        mergeIndex++;
        leftIndex++;
    }

    while (rightIndex < rightArrSize) {
        arr[mergeIndex] = rightArr[rightIndex];
        mergeIndex++;
        rightIndex++;
    }

    printArrayByRangeAndColor(arr, right + 1, left, right, LIGHT_BLUE);
}

void mergeSort(int arr[], int left, int right) {
    if (right > left) {
        int middle = (left + right) / 2;
        printArrayByRangeAndColor(arr, right + 1, left, middle, RED);
        mergeSort(arr, left, middle);
        printArrayByRangeAndColor(arr, right + 1, middle + 1, right, RED);
        mergeSort(arr, middle + 1, right);
        merge(arr, left, middle, right);
    }
}

int getValidSize(int size) {
    if (size < 1) {
        cout << "Incorrect array size! Enter correct array size:" << endl;
        cin >> size;
        if (size > 0) {
            return size;
        }
        else {
            return getValidSize(size);
        }
    }
}

int main() {
    int size, sortType;
    cout << "Enter array size:" << endl;
    cin >> size;

    size = getValidSize(size);

    cout << "Choose sort method:"<< endl<< "1. Selection sort" << endl << "2. Insertion sort" << endl << "3. Merge sort" << endl;
    cin >> sortType;

    int arr[size];
    srand(time(nullptr));
    for (int i = 0; i < size; i++) {
        arr[i] = rand() % 99 - 45;
        cout << arr[i] << " ";
    }

    cout << endl << endl;

    switch (sortType) {
        case 1:
            selectionSort(arr, size);
            break;
        case 2:
            insertionSort(arr, size);
            break;
        case 3:
            cout << "Merge sort:" << endl;
            mergeSort(arr, 0, size - 1);
        default:
            break;
    }
    cout << "Sorted array!" << endl;
    printArrayByRangeAndColor(arr, size, 0, size - 1, GREEN);

    _getch();
    return 0;
}